import React, { useEffect } from "react";
import { useValidationRules } from "../../hooks/useValidation";
import "./styles.css";

export function MyForm() {
  const { register, handleSubmit, errors, rules } = useValidationRules();

  function onSubmit(data) {
    console.log(data);
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <label>
        Email:
        <input {...register("email", { ...rules.email })} />
        {errors?.email && <span>{errors?.email?.message}</span>}
      </label>
      <label>
        Password:
        <input
          type="password"
          {...register("password", { ...rules.password })}
        />
        {errors?.password && <span>{errors?.password?.message}</span>}
      </label>
      <button type="submit">Submit</button>
    </form>
  );
}
