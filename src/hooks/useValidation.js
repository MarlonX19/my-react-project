import { useState } from "react";
import { useForm } from "react-hook-form";
import { FormValidator } from "../libs";

const rulesSet = {
  required: {
    email: {
      field: "email",
      message: "Email NECESSÁRIO",
    },
    password: {
      field: "password",
      message: "necessário senha",
    },
  },
  minLength: {},
};

export function useValidationRules() {
  const instance = new FormValidator();

  console.log("===instance", instance);

  Object.keys(rulesSet).map((values) => {
    if (values == "required") {
      Object.keys(rulesSet[values]).map((fields) => {
        console.log(Object.values(rulesSet[values][fields])[0]);
        instance
          .required(
            Object.values(rulesSet[values][fields])[0],
            Object.values(rulesSet[values][fields])[1]
          )
          .build();
      });
    }
  });

  const [rules] = useState(
    instance
      .pattern(
        "email",
        /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
        "Invalid email address."
      )
      .minLength("password", 8, "Password must be at least 8 characters.")
      .build()
  );
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    defaultValues: {},
  });
  return { register, handleSubmit, errors, rules };
}
