export class FormValidator {
  constructor() {
    this.validationRules = {};
  }

  required(fieldName, message) {
    this.validationRules[fieldName] = {
      required: { value: true, message },
    };
    return this;
  }

  minLength(fieldName, minLength, message) {
    this.validationRules[fieldName] = {
      ...this.validationRules[fieldName],
      minLength: { value: minLength, message },
    };
    return this;
  }

  maxLength(fieldName, maxLength, message) {
    this.validationRules[fieldName] = {
      ...this.validationRules[fieldName],
      maxLength: { value: maxLength, message },
    };
    return this;
  }

  pattern(fieldName, regex, message) {
    this.validationRules[fieldName] = {
      ...this.validationRules[fieldName],
      pattern: { value: regex, message },
    };
    return this;
  }

  build() {
    return this.validationRules;
  }
}
